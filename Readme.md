**VBC Stack**
Vagrant - Berkshelf - Chef
 > LEMP environment provisioned by [Chef-Solo](https://docs.chef.io/chef_solo.html) and Cookbook managed by [Berkshelf](http://berkshelf.com/). Preloaded with Ubuntu Trusty Linux with Nginx , PHP and MySQL

**Requirements**
> - [Vagrant](https://www.vagrantup.com/)
> - [ChefDK](https://downloads.chef.io/chef-dk/)
> - [Berkshelf](http://berkshelf.com/)
> - [Ruby](https://www.ruby-lang.org/en/documentation/installation/)

* Install all requirements to your host machine/local machine before you checkout this repository

**How to use**
- Clone this repo to your local folder
> $> git clone https://gitlab.com/kakisoft/vbc-stack.git vbcstack

-- Changed directory to "vbcstack"
> $> cd vbstack

-- Preload all cookbook via berkshelf
> $> berks install

-- Bring up your vagrant machine
> $> vagrant up

**Packages installed**
> * Git
> * MySQL
> * PHP
> * Nginx
> * Composer
> * Vim

**How to managed your site**
> http://10.0.0.100/

* This maps to your local vagrant folder /www/

**How manage local database**
You can use [Sequel Pro](http://www.sequelpro.com/) if you are on Mac and use the following connection parameters:
> * Connection type **SSH**
> * MySQL host : **127.0.0.1**
> * Username : **root**
> * Password : **JkvmqJRbzReX44K**
> * Database : **myapp**
> * Port : **3306**
> * SSH Host : **10.0.0.100**
> * SSH user : **vagrant**
> * SSH Password : **vagrant**