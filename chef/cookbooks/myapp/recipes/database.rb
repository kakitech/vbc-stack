mysql_service 'myapp' do
  version node['myapp']['database']['version']
  bind_address '0.0.0.0'
  initial_root_password node['myapp']['database']['password']
  action [ :create, :start ]
end

directory '/var/run/mysqld' do
  owner 'mysql'
  group 'mysql'
  mode '0755'

  action :create
end

link node['myapp']['database']['socket'] do
  link_type :symbolic
  to '/var/run/mysql-myapp/mysqld.sock'

  action :create
end

mysql2_chef_gem 'mysql' do
  action :install
end

#myapp db
mysql_database node['myapp']['database']['dbname'] do
  connection(
    :host     => node['myapp']['database']['host'],
    :username => node['myapp']['database']['username'],
    :password => node['myapp']['database']['password']
  )

  action :create
end


#grant all privileges on myapp.* to 'myapp_user'@'%'
mysql_database_user node['myapp']['database']['api']['username'] do
  connection(
    :host     => node['myapp']['database']['host'],
    :username => node['myapp']['database']['username'],
    :password => node['myapp']['database']['password']
  )

  password node['myapp']['database']['api']['password']

  database_name node['myapp']['database']['dbname']
  host '%'
  action [ :create, :grant ]
end
