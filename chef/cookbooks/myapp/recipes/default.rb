#
# Cookbook Name:: myapp
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

# Update APT
execute "apt_update" do
 command "apt-get update"
 action :run
end

# Make sure these have entries to metadata.rb
# Install the ff
include_recipe 'git'
include_recipe 'build-essential'
include_recipe 'myapp::webserver'
include_recipe 'myapp::database'
include_recipe 'myapp::firewall'
include_recipe 'php'
include_recipe 'php::module_mysql'
include_recipe 'composer'
include_recipe 'vim'


package 'php5-curl' do
  action :install
end