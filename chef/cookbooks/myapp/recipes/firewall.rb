include_recipe 'iptables'

service "firewalld" do
  action [ :stop, :disable ]
end

# see ../templates/default/ssh_firewall.rb
iptables_rule 'ssh_firewall' do
  action :enable
end

# see ../templates/default/webserver_firewall.rb
iptables_rule 'webserver_firewall' do
  action :enable
end
