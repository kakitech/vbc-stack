name             'myapp'
maintainer       'YOUR_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures myapp'
long_description 'Installs/Configures myapp'
version          '0.1.0'

# make sure cookbooks exist on Berksfile
depends 'git'
depends 'mysql'
depends 'mysql2_chef_gem'
depends 'database'
depends 'php'
depends 'nginx'
depends 'iptables'
depends 'composer'
depends 'vim'

supports 'ubuntu'

recipe 'myapp', 'Installs MyApp'
recipe 'myapp::webserver', 'Configure web server'